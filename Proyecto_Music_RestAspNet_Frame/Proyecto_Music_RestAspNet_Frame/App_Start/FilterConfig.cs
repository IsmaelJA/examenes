﻿using System.Web;
using System.Web.Mvc;

namespace Proyecto_Music_RestAspNet_Frame
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
