using System;
using System.Reflection;

namespace Proyecto_Music_RestAspNet_Frame.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}