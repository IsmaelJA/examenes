﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_Music_RestAspNet_Frame.Models
{
    [MetadataType(typeof(Music.MetaData))]
    public partial class Music
    {
        sealed class MetaData
        {
            [Key]
            public int Prodid;

            [Required(ErrorMessage = "Ingresa el nombre del albul")]
            public string Album;

            [Required(ErrorMessage = "Ingresa el nombre del artista")]
            public string Artist;

            [Required(ErrorMessage = "Ingresa el nombre del año de lanzamiento")]
            public string Release;

            [Required(ErrorMessage = "Ingresa el genero musical")]
            public string Genre;

        }
    }
}