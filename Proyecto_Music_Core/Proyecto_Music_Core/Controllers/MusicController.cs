﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Proyecto_Music_Core.Models;

namespace Proyecto_Music_Core.Controllers
{
    public class MusicController : Controller
    {
        private readonly ApplicationDbContext _db; //Se define una variable para poder acceder a la base de datos

        public MusicController(ApplicationDbContext db) // Se define Constructor - Inyeccion de dependencias 
        {
            _db = db;
        }
        public IActionResult Index()
        {
            var displaydata = _db.Music.ToList(); //Se define variable de tipo LISTA
            return View(displaydata);
        }

        // BARRA DE BUSQUEDA >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        [HttpGet]
        public async Task<IActionResult> Index(string empSearch) 
        {
            ViewData["GetMusDetails"] = empSearch;
            var empquery = from x in _db.Music select x;
            if (!String.IsNullOrEmpty(empSearch))
            {
                empquery = empquery.Where(x => x.Album.Contains(empSearch) || 
               x.Artist.Contains(empSearch));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }
        // <<<<<<<<<<<<<<<<<<<<<<< BARRA DE BUSQUEDA

        // CREATE >>>>>>>>>>>>>>>>>>>>>>>>>>
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Music nMus)
        {
            if (ModelState.IsValid) 
            {
                _db.Add(nMus); 
                await _db.SaveChangesAsync();
                return RedirectToAction("Index"); 
            }
            return View(nMus);
        }

        // <<<<<<<<<<<<<<<<<<< CREATE

        // READ >>>>>>>>>>>>>>>>>>>>>>
        public async Task<IActionResult> Detail(int? id) //Metodo para leer los datos del producto
        {
            if (id == null) //Si no existe el ID
            {
                return RedirectToAction("Index"); //Retorna a la vista Index
            }
            var getProdDetail = await _db.Music.FindAsync(id); //Se define una cariable para obtener los valores del producto seleccionado
            return View(getProdDetail);
        }
        // <<<<<<<<<<<<<<<<<<<<<<<<< READ

        // UPDATE >>>>>>>>>>>>>>>>>>>>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getProdDetail = await _db.Music.FindAsync(id);
            return View(getProdDetail);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Music oldMus)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldMus);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldMus);
        }
        // <<<<<<<<<<<<<<<<<<<<<<<<< UPDATE

        // DELETE >>>>>>>>>>>>>>>>>>>>>>>><
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("index");
            }
            var getProdDetail = await _db.Music.FindAsync(id);
            return View(getProdDetail);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getProdDetail = await _db.Music.FindAsync(id);
            _db.Music.Remove(getProdDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        // <<<<<<<<<<<<<<<<<<<<<<<<<< DELETE

    }
}
