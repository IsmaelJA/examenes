﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_Music_Core.Models
{
    public class Music
    {
        [Key]
        [Display(Name = "ID")]
        public int Prodid { get; set; }

        [Required(ErrorMessage = "Ingresa el nombre del Album")]
        [Display(Name = "Album")]
        public String Album { get; set; }

        [Required(ErrorMessage = "Ingresa el nombre del Artista")]
        [Display(Name = "Artista")]
        public String Artist { get; set; }

        [Required(ErrorMessage = "Ingresa el año de lanzamiento")]
        [Display(Name = "Año de lanzamiento")]
        public String Release { get; set; }

        [Required(ErrorMessage = "Ingresa el Generon")]
        [Display(Name = "Genero")]
        public String Genre { get; set; }

    }
}
