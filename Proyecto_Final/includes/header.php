<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>CRUD PHP - TAREAS</title>

	<!--- CDN BOOSTRAP --->

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

	<!--- CDN FONT AWESOME --->
	<script src="https://kit.fontawesome.com/a118695006.js" crossorigin="anonymous"></script>

</head>
<body>

<!---Barra navegadora --->
<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
	<div class="container">
	  <a class="navbar-brand" href="Index.php">
	    <i class="fas fa-tasks"></i>
	    Lista de tareas
	  </a>
  	</div>
</nav>

