<?php
	include("db.php");

	if (isset($_GET['id'])) {
		$id = $_GET['id'];
		$query = "SELECT * FROM tareas
					WHERE id = $id";
		$result = mysqli_query($conexion, $query);

		if(mysqli_num_rows($result) == 1){
			//echo "puedes editar";
			$row = mysqli_fetch_array($result);
			$title = $row['titulo'];
			$description = $row['descripcion'];
		}
	}

	if(isset($_POST['update'])){
		//echo "Actualizando";
		$id = $_GET['id'];
		$title = $_POST['title'];
		$description = $_POST['description'];

		$query = "UPDATE tareas set titulo = '$title',
									descripcion = '$description'
					WHERE  id= $id";
		mysqli_query($conexion, $query);

		$_SESSION['message'] = 'Se actualizó la tarea correctamente';
		$_SESSION['message_type'] = 'primary';

		header("Location: Index.php");
	}
?>

<?php include("includes/header.php"); ?>

<div class="container p-4">
	<div class="row">
		<div class="col-md-4 mx-auto">
			<div class="card card-body">
				<form action="edit.php?id=<?php echo $_GET['id'];  ?>" method="POST">
					<div class="form-group">
						<input type="text" name="title" value="<?php echo $title; ?>"
						class="form-control" placeholder="Actualiza el titulo">
					</div> 
					<div class="form-group">
						<textarea name="description" rows="2" class="form-control" 
						placeholder="Actualiza la descripcion"><?php echo $description; ?></textarea>
						
					</div> 
					<button class="btn btn-success" name="update">
						Actualizar
					</button>
				</form>
				
			</div>
			
		</div>
		
	</div>
	

</div>

<?php include("includes/footer.php"); ?>