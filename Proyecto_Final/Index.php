<?php 
//Archivo que arrancara la applicacion 
//Se requiere inicializar la conexion con la base de datos

include("db.php"); //Archivo de conexion con la base de datos
include("includes/header.php"); //Se incluye el header ubicado en la carpeta includes
?>

<div class="container p-4">
	<div class="row">
		<div class="col-md-4">

			<?php if(isset($_SESSION['message'])){   ?>
				<!--- Mensaje de alerta --->
				<div class="alert alert-<?= $_SESSION['message_type']; ?> alert-dismissible fade show" role="alert">
  				<?= $_SESSION['message'] ?>
 				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    			<span aria-hidden="true">&times;</span>
  				</button>
				</div>


			<?php session_unset();}?>

			<div class="card card-body">
				<form action="save_task.php" method="POST"> <!-- Se especifica donde se van a enviar los datos del formulario y por que metodo  -->
					<div class="form-group">
						<p> <strong>Agregar tarea </strong></p>
					</div>
					<div class="form-group">
						<input type="text" name="title" class="form-control"
						placeholder="Titulo de la tarea" autofocus>	
					</div>
					<div class="form-group">
						<textarea name="description" rows="2" class="form-control"
						placeholder="Descripcion de la tarea..."></textarea>
					</div>
					<input type="submit" class="btn btn-success btn-block" name="save_task"
					value="Guardar">
				</form>
			</div>
			
		</div>

		<div class="col-md-8">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Titulo</th>
						<th>Descripcion</th>
						<th>Fecha</th>
						<th>Acciones</th>
						
					</tr>
					
				</thead>
				<tbody>
					<?php

					$query = "SELECT * FROM tareas";
					$result_task = mysqli_query($conexion, $query); // Esta variable va a lmacenar los registros de tareas

					while($row = mysqli_fetch_array($result_task)){ ?>
						<tr>
							<td><?php echo $row['titulo']; ?></td>
							<td><?php echo $row['descripcion']; ?></td>
							<td><?php echo $row['fecha']; ?></td>
							<td>
								<a href="edit.php?id=<?php echo $row['id']; ?>" class="btn 
									btn-secondary">
									<i class="fas fa-pencil-alt"></i>
								</a>
								<a href="delete_task.php?id=<?php echo $row['id']; ?>" class="btn 
									btn-danger">
									<i class="fas fa-calendar-times"></i>
								</a> 

							</td>
						</tr>
						

					<?php } ?>

					
				</tbody>
				
			</table>

		</div>


	</div>
	


</div>


<?php include("includes/footer.php"); //Se incluye el footer ubicado en la carpeta includes?>