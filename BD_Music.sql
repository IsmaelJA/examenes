CREATE DATABASE MUS;
USE MUS;

CREATE TABLE Music(
	Prodid int not null primary key identity(1,1),
	Album nvarchar(150),
	Artist nvarchar(150),
	Release nvarchar(150),
	Genre nvarchar(150),
);

Select * from Music;

insert into Music
values('Los Amsterdam', 'Yellow Claw', '2017','Dance');